
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Created by Vitaliq on 22.08.17.
 */

@RunWith(Parameterized.class)
public class CalcTest {

    String a;
    String b;
//    char sign;
    static CalcPageObject calc = new CalcPageObject();

    public CalcTest (String a, String b){

        this.a = a;
        this.b = b;
//        this.sign = sign;

    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                { "2", "4" },
//                { "-1", "-10"},
//                { "-3", "12"},
//                { "5", "-1"},
                { "2.1", "4.2" },
//                { "-1.3", "-10.4"},
//                { "-3.5", "12.6"},
//                { "5.7", "-1.8"},
                { "0", "4" },
                { "1", "0"},
                { "0", "0"},
        };

        return Arrays.asList(data);
    }

    @BeforeClass
    public static void setUp() throws InterruptedException, IOException, ParseException {

        JSONObject jsonObject = (JSONObject) new JSONParser().parse(new FileReader(
                "settings.json"));

        String deviceName = (String) jsonObject.get("Device name");
        String osVersion = (String) jsonObject.get("OS version");
        String appiumUrl = (String) jsonObject.get("Appium URL");
        String appDir = (String) jsonObject.get("App dir");

        calc = new CalcPageObject();
        calc.setUp(deviceName, osVersion, appiumUrl, appDir);

    }

    @After
    public void reset(){

        calc.reset();

    }

    @AfterClass
    public static void close(){

        calc.close();

    }

    @Test
    public void plus(){

        calc.setData(this.a, this.b);
        calc.setSign('+');
        System.out.println(this.a + " + " + this.b +" = "+calc.getNumberResult());
        String res = calc.getNumberResult();
        res = res.substring(0, res.indexOf(',')) + "." + res.substring(res.indexOf(',')+1);
        assertEquals(Float.parseFloat(this.a) + Float.parseFloat(this.b),
                Float.parseFloat(res),
                0.001);

    }

    @Test
    public void minus(){

        calc.setData(this.a, this.b);
        calc.setSign('-');
        System.out.println(this.a + " - " + this.b +" = " + calc.getNumberResult());
        String res = calc.getNumberResult();
        res = res.substring(0, res.indexOf(',')) + "." + res.substring(res.indexOf(',')+1);
        assertEquals(Float.parseFloat(this.a) - Float.parseFloat(this.b),
                Float.parseFloat(res),
                0.001);

    }

    @Test
    public void div(){

        calc.setData(this.a, this.b);
        calc.setSign('/');
        System.out.println(this.a + " / " + this.b +" = "+calc.getNumberResult());
        if(this.b.compareTo("0")==0)
            assertEquals("Infinity", calc.getNumberResult());
        else {
            String res = calc.getNumberResult();
            res = res.substring(0, res.indexOf(',') ) + "." + res.substring(res.indexOf(',') + 1);
            assertEquals(Float.parseFloat(this.a) / Float.parseFloat(this.b),
                    Float.parseFloat(res),
                    0.001);
        }

    }

    @Test
    public void mul(){

        calc.setData(this.a, this.b);
        calc.setSign('*');
        System.out.println(this.a + " * " + this.b +" = "+calc.getNumberResult());
        String res = calc.getNumberResult();
        res = res.substring(0, res.indexOf(',')) + "." + res.substring(res.indexOf(',')+1);
        assertEquals(Float.parseFloat(this.a) * Float.parseFloat(this.b),
                Float.parseFloat(res),
                0.001);

    }



}
