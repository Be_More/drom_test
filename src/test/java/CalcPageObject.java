import io.appium.java_client.android.AndroidDriver;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * Created by Vitaliq on 23.08.17.
 */
public class CalcPageObject {

    private static AndroidDriver driver;

    public void setUp(String device, String osVersion, String appiumUrl, String appDir) throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability("deviceName", device);
        capabilities.setCapability("platformVersion", osVersion);
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("app", appDir);
        capabilities.setCapability("appPackage", "com.vbanthia.androidsampleapp");
        capabilities.setCapability("appActivity", "com.vbanthia.androidsampleapp.MainActivity");

        driver = new AndroidDriver(new URL(appiumUrl), capabilities);
        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
    }

    public void setData(String a, String b) {

        driver.findElement(By.xpath("//android.widget.EditText[@content-desc= 'inputFieldLeft']"))
                .sendKeys(a);
        driver.findElement(By.xpath("//android.widget.EditText[@content-desc= 'inputFieldRight']"))
                .sendKeys(b);

    }

    public void setSign(char sign) {

        switch (sign) {

            case '+':
                driver.findElement(By.xpath("//android.widget.Button[@text= '+']"))
                        .click();
                break;

            case '-':
                driver.findElement(By.xpath("//android.widget.Button[@text= '-']"))
                        .click();
                break;

            case '*':
                driver.findElement(By.xpath("//android.widget.Button[@text= '*']"))
                        .click();
                break;

            case '/':
                driver.findElement(By.xpath("//android.widget.Button[@text= '/']"))
                        .click();
                break;

            default:
                throw new java.lang.IllegalArgumentException("Invalid sign.");

        }


    }

    public String getResult() {

        return driver.findElement(By.xpath("//android.widget.TextView")).getText();

    }

    public String getNumberResult() {

        String res = driver.findElement(By.xpath("//android.widget.TextView")).getText();
        return res.substring(res.indexOf('=') + 2);

    }

    public void reset() {

        driver.findElement(By.xpath("//android.widget.Button[@text = 'RESET']"))
                .click();

    }

    public void close() {

        driver.quit();

    }

}
